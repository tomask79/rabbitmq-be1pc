# Spring AMQP with RabbitMQ, part 6 #

## Best Effort 1PC pattern with Spring AMPQ ##

What is Best Effort 1PC pattern? Okay, lets consider you use SimpleMessageListenerContainer for asynchronous messages reading from RabbitMQ queue, then Best Effort 1PC pattern means:

1. receive message (you get message as method input)
2. write DB (you call some JPA method to write message processing result into DB)
3. commit DB (Spring performes commit if you've got Spring Transaction ongoing)
4. ACK message (Spring AMPQ sends ack to RabbitMQ to acknowledge message was processed)

And what's the catch in Best Effort 1PC pattern? Last two steps Spring puts close together as much as he can.
To avoid message loosing.

## Howto configure BE 1PC with Spring AMPQ ##

**Asynchrounous receive listener:**
```
	@Bean
	public SimpleMessageListenerContainer transactionalListener() {
		SimpleMessageListenerContainer simpleMessageListenerContainer = new SimpleMessageListenerContainer();
		simpleMessageListenerContainer.setConnectionFactory(connectionFactory());
		simpleMessageListenerContainer.setAcknowledgeMode(AcknowledgeMode.AUTO);
		simpleMessageListenerContainer.setChannelTransacted(true);
		simpleMessageListenerContainer.setupMessageListener(new MessageListenerAdapter(receiver(), "receive"));
		simpleMessageListenerContainer.setQueues(fruits());
		simpleMessageListenerContainer.setConcurrentConsumers(1);
		simpleMessageListenerContainer.setMaxConcurrentConsumers(10);
		simpleMessageListenerContainer.setDefaultRequeueRejected(false);
		return simpleMessageListenerContainer;
	}
```
* We go with AUTO ack. When transactional receive method ends OK, ack is send to RabbitMQ
* setChannelTransacted to true is necessary to have BE 1PC pattern working with RabbitMQ
* Concurrency is set at 1-10 threads

** Message processing service opening JPA transaction when receiving message:**
```
/**
 * @author Tomas Kloucek
 */
public class RabbitMqReceiver {

	@Autowired
	protected ItemsService itemsService;

	@Transactional
	public void receive(String item) throws Exception{
		System.out.println(" [x] Received '" + item + "'");
		//throw new Exception("Error");
		itemsService.processItem(item);
	}
```

This is the typical message processing you usually want to do when receiving messages asynchronously.

## Testing the demo ##

I use OracleXE for saving the messages. Change JDBC URL according to your installation in following Bean:


```
	@Bean
	public DataSource dataSource() throws SQLException{
		OracleDataSource ds = new OracleDataSource();
		ds.setDataSourceName("OracleDS");
		ds.setURL("jdbc:oracle:thin:@localhost:1521:xe");
		ds.setUser("TOMAS");
		ds.setPassword("TOMAS123");
		return ds;
	}
```


* git clone <this repo>
* mvn clean install
* java -jar -Dspring.profiles.active=receiver target/demo-0.0.1-SNAPSHOT.jar

Now try to send something into default exchange where both queues fruits and vegetables are binded.
Message("This is a test!", for example) then should be processed with output like:


```
2017-01-29 12:46:54.502  INFO 4336 --- [           main] demo_rabbitmq.rabbit_demo.App            : Started App in 5.129 seconds (JVM running for 5.486)
 [x] Received 'This is a test!'
Hibernate: select item0_.ID as ID1_0_0_, item0_.ITEM_NAME as ITEM_NAME2_0_0_ from ITEMS item0_ where item0_.ID=?
Hibernate: insert into ITEMS (ITEM_NAME, ID) values (?, ?)
```

## Dead letter exchange ##

Okay, and what to do with unprocessed messages if something will go wrong in the receive method? RabbitMQ offers dead letter exchanges...So lets create exchange with "**dead-letter-exchange**" name and fanout type first. Then change the creation of your queue to point it to the created dead letter exchange:


```
@Bean
	public Queue fruits() {
		Map<String, Object> args = new HashMap<String, Object>();
		args.put("x-dead-letter-exchange", "dead-letter-exchange");
		return new Queue("fruits", true, false, false, args);
	}
```
For the testing purposes, change the receive method to:

```
@Transactional
	public void receive(String item) throws Exception{
		System.out.println(" [x] Received '" + item + "'");
		throw new Exception("Error");
		//itemsService.processItem(item);
	}
```
**Important!** Do not forget to **set simpleMessageListenerContainer.setDefaultRequeueRejected(false) ** Otherwise unprocessed message will be put into queue again instead of to dead letter queue. Now try to send something into fruits queue:

Output (expected crash):

```
 [x] Received 'This is a dead letter exchange test!'
2017-01-29 12:58:47.378  WARN 13756 --- [cTaskExecutor-1] s.a.r.l.ConditionalRejectingErrorHandler : Execution of Rabbit message listener failed.

org.springframework.amqp.rabbit.listener.exception.ListenerExecutionFailedException: Listener method 'receive' threw exception
        at org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter.invokeListenerMethod(MessageListenerAdapter.java:395) ~[s
```
Now check queue you pointed to your dead letter exchange. It should contain the unprocessed message with payload 'This is a dead letter exchange test!. 

```
Exchange	dead-letter-exchange
Routing Key	fruits
Redelivered	○
Properties	
delivery_mode:	1
headers:	
x-death:	
count:	1
reason:	rejected
queue:	fruits
time:	1485691127
exchange:	
routing-keys:	fruits
content_type:	text/plain
Payload
36 bytes
Encoding: string
This is a dead letter exchange test
```

Remember, message will be dead lettered if:

* Timeout will occur
* Message will be rejected from queue
* Queue length limit will be reached

regards

Tomas