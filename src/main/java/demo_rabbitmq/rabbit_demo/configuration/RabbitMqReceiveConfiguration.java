package demo_rabbitmq.rabbit_demo.configuration;

import demo_rabbitmq.rabbit_demo.services.ItemsService;
import oracle.jdbc.pool.OracleDataSource;
import org.springframework.amqp.core.AcknowledgeMode;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import demo_rabbitmq.rabbit_demo.annotations.Receiver;
import demo_rabbitmq.rabbit_demo.receiver.RabbitMqReceiver;
import org.springframework.orm.jpa.AbstractEntityManagerFactoryBean;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@Receiver
@Configuration
public class RabbitMqReceiveConfiguration {
	
	{
		System.out.println("Creating receiving configuration.");
	}
	
	@Bean
	public ConnectionFactory connectionFactory() {
		CachingConnectionFactory connectionFactory = 
				new CachingConnectionFactory("localhost");
		connectionFactory.setUsername("guest");
		connectionFactory.setPassword("guest");
		return connectionFactory;
	}
	
	@Bean
	public AmqpAdmin amqpAdmin() {
		return new RabbitAdmin(connectionFactory());
	}
	
	@Bean
	public Queue fruits() {
		Map<String, Object> args = new HashMap<String, Object>();
		args.put("x-dead-letter-exchange", "dead-letter-exchange");
		return new Queue("fruits", true, false, false, args);
	}
	
	@Bean
	public Queue vegetables() {
		final Queue queue = new Queue("vegetables");
		return queue;
	}
	
	@Bean
	public RabbitMqReceiver receiver() {
		return new RabbitMqReceiver();
	}

	@Bean
	public ItemsService personService() throws SQLException{
		return new ItemsService();
	}

	@Bean
	public DataSource dataSource() throws SQLException{
		OracleDataSource ds = new OracleDataSource();
		ds.setDataSourceName("OracleDS");
		ds.setURL("jdbc:oracle:thin:@localhost:1521:xe");
		ds.setUser("TOMAS");
		ds.setPassword("TOMAS123");
		return ds;
	}

	@Bean
	public JpaTransactionManager transactionManager() throws SQLException{
		JpaTransactionManager jpaTransactionManager = new JpaTransactionManager();
		jpaTransactionManager.setEntityManagerFactory(entityManagerFactory().getObject());
		return jpaTransactionManager;
	}

	@Bean
	public AbstractEntityManagerFactoryBean entityManagerFactory() throws SQLException{
		LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
		factory.setPersistenceUnitName("rabbitmq-transactions");
		factory.setJpaVendorAdapter(jpaVendorAdapter());
		factory.setDataSource(dataSource());
		return factory;
	}

	@Bean
	public JpaVendorAdapter jpaVendorAdapter() {
		HibernateJpaVendorAdapter hibernateJpaVendorAdapter = new HibernateJpaVendorAdapter();
		hibernateJpaVendorAdapter.setShowSql(true);
		return hibernateJpaVendorAdapter;
	}

	@Bean
	public SimpleMessageListenerContainer transactionalListener() {
		SimpleMessageListenerContainer simpleMessageListenerContainer = new SimpleMessageListenerContainer();
		simpleMessageListenerContainer.setConnectionFactory(connectionFactory());
		simpleMessageListenerContainer.setAcknowledgeMode(AcknowledgeMode.AUTO);
		simpleMessageListenerContainer.setChannelTransacted(true);
		simpleMessageListenerContainer.setupMessageListener(new MessageListenerAdapter(receiver(), "receive"));
		simpleMessageListenerContainer.setQueues(fruits());
		simpleMessageListenerContainer.setConcurrentConsumers(1);
		simpleMessageListenerContainer.setMaxConcurrentConsumers(10);
		simpleMessageListenerContainer.setDefaultRequeueRejected(false);
		return simpleMessageListenerContainer;
	}
}
